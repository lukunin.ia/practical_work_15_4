#include <iostream>

using namespace std;

void FindEvenNumbers ()
{
	const int N = 13;
	for (int i = 0; i < N + 1; ++i)
	{
		if (i % 2 == 0)
		{
			cout << i << endl;
		}
	}
}

int main()
{
	FindEvenNumbers();
	return 0;
}